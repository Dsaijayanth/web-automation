import smtplib
import traceback
from constants import Constants
server = None


def login():
    global server
    try:
        server = smtplib.SMTP_SSL(Constants.SMTP_GMAIL)
        server.login(Constants.FROM_EMAIL, Constants.EMAIL_PASS)
    except Exception as e:
        print(traceback.format_exc(), str(e), "while login")


def send_email(to, price, existing_price, title, name):
    body = Constants.EMAIL_BODY.format(
        name=name, product=title, old_price=existing_price, new_price=price)
    try:
        server.sendmail(Constants.FROM_EMAIL, to, body)
        print("email sent to {name} successfully".format(name=name))
    except Exception as e:
        print(traceback.format_exc(), str(e), "while sending mail")


def close():
    try:
        server.close()
    except Exception as e:
        print(traceback.format_exc(), str(e), "while closing server")
