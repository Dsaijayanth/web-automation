### SAI JAYANTH DEVIREDDY

I go by *sai*

Currently pursuing my __masters in computer science__

[Here is my Linkedin profile](https://www.linkedin.com/in/sai-jayanth-16460b77/) 

- Experience in _c++_ - 0
- Experience in _git_ - 1.5 years

* Languages familiar with
    * Python
    * Core Java
    * JavaScript
    * Node JS

* Courses taken so far
    * Spring 2021
        * CS 5120 - Design and Analysis of Algorithms
        * CS 5330 - Network Security and Forensics
        * CS 5850 - Redings in Computer Science
    * Fall 2021
        * CS 5050 - Research Methods in Computer Science
        * CS 6010 - Data Science Programming
        * SE 5550 - Software Architecture & Design
        * SE 5560 - Software Testing & Quality Assurance

I am from India. I did my undergrad in Electronics and Communications in 2016. Since then I worked for 3 years in Infosys as system engineer. During my stint at Infosys I worked with Verizon in devloping ChatBot using Node JS. Later in 2019 I switched to a startup called CarDekho. At that time I worked on Python developing an alogirthm to estimate price of a used car based on the past history. To the end of 2020 I decided to pursue my masters and here I am.

__Fun fact__ - I love watching sports!