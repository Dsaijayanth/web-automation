#set latest or requiered python version
FROM python:latest
#copy code in to the container
COPY . /web-automation
#set the working directory
WORKDIR /web-automation
#install packages using pip
RUN pip install -r requirements.txt
#expose the port you want your API to run on
EXPOSE 8050
#run the script that ups your API.
CMD python3 ./get_price.py