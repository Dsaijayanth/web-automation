import gspread
from oauth2client.service_account import ServiceAccountCredentials
import datetime
from constants import Constants

creds = ServiceAccountCredentials.from_json_keyfile_name(
    'client-secret.json', Constants.SCOPE)
client = gspread.authorize(creds)
sheet = client.open(Constants.SHEET_NAME).sheet1
data = sheet.get_all_records()


def get_data():
    global data
    data = sheet.get_all_records()
    return data


def insert_data(new_record):
    sheet.insert_row(new_record, len(data)+1)


def delete_row(index):
    sheet.delete_row(index)


def update_price(price_array, price_list, time_line):
    length = str(len(data)+1)
    cell_list = sheet.range('C2:C'+length)
    last_updated = sheet.range('F2:F'+length)
    existing_price_list = sheet.range('G2:G'+length)
    existing_time_line = sheet.range('H2:H'+length)
    now = datetime.datetime.now()
    for i, val in enumerate(price_array):
        cell_list[i].value = val
        last_updated[i].value = str(now)
        existing_price_list[i].value = str(price_list[i])
        existing_time_line[i].value = str(time_line[i])

    sheet.update_cells(cell_list)
    sheet.update_cells(last_updated)
    sheet.update_cells(existing_price_list)
    sheet.update_cells(existing_time_line)
