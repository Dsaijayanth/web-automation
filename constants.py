class Constants:
    FROM_EMAIL = "pythonwebautomation1305@gmail.com"
    EMAIL_PASS = "Webtrack@1305"
    EMAIL_BODY = "\nHi {name}, \nThe price of {product} your looking is changed from {old_price} to {new_price}."
    SMTP_GMAIL = "smtp.gmail.com"
    PRICE_BLOCK  = "priceblock_ourprice"
    TEXT_BLOCK = "productTitle"
    HEADERS = {"User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36"}
    SHEET_NAME = "price_tracking"
    SCOPE = ['https://www.googleapis.com/auth/spreadsheets','https://www.googleapis.com/auth/drive']
    ITEM_URL = "url"
    ITEM_PRICE = "price"
    ITEM_TITLE = "title"
    USER_NAME = "name"
    USER_EMAIL = "email"
    INTERVAL_TIME = 3
    ITEM_PRICE_LIST = "price_list"
    ITEM_TIME_LINE = "time_line"