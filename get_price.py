from flask import Flask, request
import json
from constants import Constants
from apscheduler.schedulers.background import BackgroundScheduler
from selenium import webdriver
from selenium.common.exceptions import *
import spreadhseet as spreadsheet
import re
import send_email
import datetime
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as pl
import pandas as pd


app = Flask(__name__)
scheduler = BackgroundScheduler()
scheduler.start()
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--headless')
browser = None

dash_app = dash.Dash(
    __name__,
    server=app,
    routes_pathname_prefix='/product/'
)
server = dash_app.server
dash_app.layout = html.Div(id='example-div-element')


@app.route('/')
def hello():
    return 'Hello World!'


@app.route('/start')
def start_app():
    update_price()
    return {'message': 'success'}, 200


@app.route('/product/history')
def dash_test():
    product_list = spreadsheet.get_data()
    options = []
    for product in product_list:
        if product[Constants.ITEM_TITLE] != "":
            option_obj = {
                'label': product[Constants.ITEM_TITLE], 'value': product[Constants.ITEM_TITLE]}
            options.append(option_obj)
    dash_app.layout = html.Div(children=[html.H1(children='Product Price Time-Line'), dcc.Dropdown(id='product',
                                                                                                   options=options,
                                                                                                   value=options[0]['value']
                                                                                                   ), html.Div(id='timeline')])

    return dash_app.index()


@dash_app.callback(dash.dependencies.Output('timeline', 'children'), [dash.dependencies.Input('product', 'value')])
def dash_interactive_test(value):
    product_list = spreadsheet.get_data()
    result = filter(lambda x: x[Constants.ITEM_TITLE] == value, product_list)
    if result != None:
        result = list(result)[0]
        if result[Constants.ITEM_TIME_LINE] != "":
            price_list_timeline = pd.DataFrame({'Date': eval(
                result[Constants.ITEM_TIME_LINE]), 'Price': eval(result[Constants.ITEM_PRICE_LIST])})
            fig = pl.scatter(data_frame=price_list_timeline,
                             x='Date', y='Price')
            dcc.Graph(figure=fig)
        else:
            return "There is no history present for this product"
    else:
        return "There is a problem in rendering Graph"
    return dcc.Graph(figure=fig)


@app.route('/price_track/add_item', methods=['POST'])
def add_item(item=None):
    """ while inserting get latest price and title before inserting it to the sheet """
    if item == None:
        item = request.data
    print(item)
    if item:
        item = json.loads(item)
        new_record = []
        new_record.append(item[Constants.ITEM_URL])
        title, price = get_data_by_selenium(item[Constants.ITEM_URL])
        new_record.append(title)
        new_record.append(price)
        new_record.append(item[Constants.USER_EMAIL])
        new_record.append(item[Constants.USER_NAME])
        now = datetime.date.today()
        new_record.append(str(now))
        new_record.append(str([price]))
        date = now.strftime("%Y-%m-%d")
        new_record.append(str([date]))
        spreadsheet.insert_data(new_record)
    return {"message": "success"}, 200


@app.route('/price_track/remove_item', methods=['DELETE'])
def remove_item(item=None):
    product_list = spreadsheet.get_data()
    if item == None:
        item = request.json
    if item:
        for i in range(len(product_list)):
            if product_list[i][Constants.ITEM_URL] == item[Constants.ITEM_URL]:
                spreadsheet.delete_row(i+2)
                break
    return {"message": "success"}, 204


@scheduler.scheduled_job('interval', hours=Constants.INTERVAL_TIME)
def update_price():
    global browser
    browser = webdriver.Chrome(options=chrome_options)
    product_list = spreadsheet.get_data()
    new_price = []
    price_list = []
    time_line = []
    print("triggered for price check")
    send_email.login()
    now = datetime.date.today()
    date = now.strftime("%Y-%m-%d")
    for product in product_list:
        title, price = get_data_by_selenium(product[Constants.ITEM_URL])
        existing_price = product[Constants.ITEM_PRICE]
        existing_price_list = product[Constants.ITEM_PRICE_LIST]
        existing_time_line = product[Constants.ITEM_TIME_LINE]
        if existing_price and price != 0:
            percent_diff = ((int(existing_price)-int(price))
                            * 100)/int(existing_price)
            if percent_diff >= 20:
                send_email.send_email(product[Constants.USER_EMAIL], price, existing_price,
                                      product[Constants.ITEM_TITLE], product[Constants.USER_NAME])
            if existing_price_list == '':
                existing_price_list = [price]
            else:
                existing_price_list = eval(existing_price_list)
                existing_price_list.append(price)
            if existing_time_line == '':
                existing_time_line = [date]
            else:
                existing_time_line = eval(existing_time_line)
                existing_time_line.append(date)
        price_list.append(existing_price_list)
        time_line.append(existing_time_line)
        new_price.append(price)
    send_email.close()
    spreadsheet.update_price(new_price, price_list, time_line)
    browser.quit()


def get_data_by_selenium(url=None):
    title = None
    price = 0
    if url:
        browser.get(url)
        if find_elemnt_exists(Constants.TEXT_BLOCK):
            title = browser.find_element_by_id(Constants.TEXT_BLOCK).text
        if find_elemnt_exists(Constants.PRICE_BLOCK):
            price = browser.find_element_by_id(Constants.PRICE_BLOCK).text
            if price != None:
                price = price.replace(",", "")
                price = int(re.search('[0-9]+', price).group())
    return title, price


def find_elemnt_exists(element):
    try:
        browser.find_element_by_id(element)
        return True
    except NoSuchElementException:
        return False


if __name__ == "__main__":
    dash_app.run_server(debug=True, host='0.0.0.0')
